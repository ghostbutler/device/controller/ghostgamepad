package service

import (
	"errors"
	"github.com/splace/joysticks"
	"github.com/streadway/amqp"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"math"
	"reflect"
	"strconv"
	"time"
)

type EventType int

const (
	EventTypeButtonPressed = EventType(iota)
	EventTypeButtonReleased
	EventTypeButtonDoublePressed
	EventTypeButtonLongPressed
	EventTypeJoystickMove
)

const (
	JoystickUpdateThreshold = 0.1
)

type EventInfo struct {
	Type                 EventType
	Index                int
	PreviousX, PreviousY float32
}

type Gamepad struct {
	// rabbit mq controller
	rabbitMQController *rabbit.Controller

	// base name
	BaseName string

	// is running?
	isRunning bool

	// device
	gamepad *joysticks.HID

	// events channels
	Event []chan joysticks.Event

	// joysticks count
	JoystickCount int

	// buttons count
	ButtonCount int

	// channels details
	ChannelInfo []EventInfo

	// index
	index int
}

// build gamepad
func BuildGamepad(index int,
	rabbitMQController *rabbit.Controller,
	baseName string) (*Gamepad, error) {
	gamepad := &Gamepad{
		BaseName: baseName,
		Event: make([]chan joysticks.Event,
			0,
			1),
		ChannelInfo: make([]EventInfo,
			0,
			1),
		index:              index,
		isRunning:          true,
		rabbitMQController: rabbitMQController,
	}
	if dvc := joysticks.Connect(index); dvc == nil {
		return nil, errors.New("can't connect gamepad " +
			strconv.Itoa(index))
	} else {
		gamepad.gamepad = dvc
		i := 1
		for ; dvc.ButtonExists(uint8(i)); i++ {
			gamepad.Event = append(gamepad.Event,
				dvc.OnClose(uint8(i)))
			gamepad.ChannelInfo = append(gamepad.ChannelInfo,
				EventInfo{
					Type:  EventTypeButtonPressed,
					Index: i,
				})
			gamepad.Event = append(gamepad.Event,
				dvc.OnOpen(uint8(i)))
			gamepad.ChannelInfo = append(gamepad.ChannelInfo,
				EventInfo{
					Type:  EventTypeButtonReleased,
					Index: i,
				})
			gamepad.Event = append(gamepad.Event,
				dvc.OnDouble(uint8(i)))
			gamepad.ChannelInfo = append(gamepad.ChannelInfo,
				EventInfo{
					Type:  EventTypeButtonDoublePressed,
					Index: i,
				})
			gamepad.Event = append(gamepad.Event,
				dvc.OnLong(uint8(i)))
			gamepad.ChannelInfo = append(gamepad.ChannelInfo,
				EventInfo{
					Type:  EventTypeButtonLongPressed,
					Index: i,
				})
		}
		gamepad.ButtonCount = i - 1
		i = 1
		for ; dvc.HatExists(uint8(i)); i++ {
			gamepad.Event = append(gamepad.Event,
				dvc.OnMove(uint8(i)))
			gamepad.ChannelInfo = append(gamepad.ChannelInfo,
				EventInfo{
					Type:  EventTypeJoystickMove,
					Index: i,
				})
		}
		gamepad.JoystickCount = i - 1
	}
	go gamepad.gamepad.ParcelOutEvents()
	go gamepad.update()
	return gamepad, nil
}

// close
func (gamepad *Gamepad) Close() {
	// close all channels
	gamepad.isRunning = false
	for _, channel := range gamepad.Event {
		close(channel)
	}
}

// send event to rabbit
func (gamepad *Gamepad) sendEvent(data []byte) error {
	if channel := gamepad.rabbitMQController.Channel; channel != nil {
		return channel.Publish("",
			rabbit.SensorQueueName,
			false,
			false,
			amqp.Publishing{
				ContentType: "application/json",
				Body:        data,
			})
	} else {
		return errors.New("no channel available")
	}
}

// update
func (gamepad *Gamepad) update() {
	// build reflect select cases
	cases := make([]reflect.SelectCase,
		len(gamepad.Event))
	for i, ch := range gamepad.Event {
		cases[i] = reflect.SelectCase{
			Dir:  reflect.SelectRecv,
			Chan: reflect.ValueOf(ch),
		}
	}

	// thread
	for gamepad.isRunning {
		if chosen, value, ok := reflect.Select(cases); ok {
			event := device.Switch{
				Type:        device.SwitchEventTypeButton,
				UUID:        gamepad.BaseName,
				IsReachable: true,
				LastUpdate:  time.Now().Format(time.RFC3339Nano),
			}
			switch gamepad.ChannelInfo[chosen].Type {
			case EventTypeButtonPressed:
				event.Name = gamepad.BaseName +
					".gamepad." +
					strconv.Itoa(gamepad.index) +
					".button." +
					strconv.Itoa(gamepad.ChannelInfo[chosen].Index)
				event.Event = device.Button{
					Name: gamepad.BaseName +
						".gamepad." +
						strconv.Itoa(gamepad.index) +
						".button." +
						strconv.Itoa(gamepad.ChannelInfo[chosen].Index),
					Status: device.ButtonStatusPressed,
				}
				_ = gamepad.sendEvent(event.MarshalJson())
				break
			case EventTypeButtonReleased:
				event.Name = gamepad.BaseName +
					".gamepad." +
					strconv.Itoa(gamepad.index) +
					".button." +
					strconv.Itoa(gamepad.ChannelInfo[chosen].Index)
				event.Event = device.Button{
					Name: gamepad.BaseName +
						".gamepad." +
						strconv.Itoa(gamepad.index) +
						".button." +
						strconv.Itoa(gamepad.ChannelInfo[chosen].Index),
					Status: device.ButtonStatusReleased,
				}
				_ = gamepad.sendEvent(event.MarshalJson())
				break
			case EventTypeButtonDoublePressed:
				event.Name = gamepad.BaseName +
					".gamepad." +
					strconv.Itoa(gamepad.index) +
					".button." +
					strconv.Itoa(gamepad.ChannelInfo[chosen].Index)
				event.Event = device.Button{
					Name: gamepad.BaseName +
						".gamepad." +
						strconv.Itoa(gamepad.index) +
						".button." +
						strconv.Itoa(gamepad.ChannelInfo[chosen].Index),
					Status: device.ButtonStatusDoublePressed,
				}
				_ = gamepad.sendEvent(event.MarshalJson())
				break
			case EventTypeButtonLongPressed:
				event.Name = gamepad.BaseName +
					".gamepad." +
					strconv.Itoa(gamepad.index) +
					".button." +
					strconv.Itoa(gamepad.ChannelInfo[chosen].Index)
				event.Event = device.Button{
					Name: gamepad.BaseName +
						".gamepad." +
						strconv.Itoa(gamepad.index) +
						".button." +
						strconv.Itoa(gamepad.ChannelInfo[chosen].Index),
					Status: device.ButtonStatusLongPressed,
				}
				_ = gamepad.sendEvent(event.MarshalJson())
				break
			case EventTypeJoystickMove:
				var x, y float32
				switch value.Interface().(type) {
				case joysticks.CoordsEvent:
					x = value.Interface().(joysticks.CoordsEvent).X
					y = value.Interface().(joysticks.CoordsEvent).Y

					deltaX := math.Abs(float64(x - gamepad.ChannelInfo[chosen].PreviousX))
					deltaY := math.Abs(float64(y - gamepad.ChannelInfo[chosen].PreviousY))

					if deltaX >= JoystickUpdateThreshold ||
						deltaY >= JoystickUpdateThreshold {
						event.Type = device.SwitchEventTypeJoystick
						event.Name = gamepad.BaseName +
							".gamepad." +
							strconv.Itoa(gamepad.index) +
							".joystick." +
							strconv.Itoa(gamepad.ChannelInfo[chosen].Index)
						event.Event = device.Joystick{
							Name: gamepad.BaseName +
								".gamepad." +
								strconv.Itoa(gamepad.index) +
								".joystick." +
								strconv.Itoa(gamepad.ChannelInfo[chosen].Index),
							X: float64(x),
							Y: float64(y),
						}
						_ = gamepad.sendEvent(event.MarshalJson())
						gamepad.ChannelInfo[chosen].PreviousX = x
						gamepad.ChannelInfo[chosen].PreviousY = y
					}
					break
				}

				break
			}
		}
		time.Sleep(time.Millisecond * 16)
	}
}

func (gamepad *Gamepad) SetBaseName(base string) {
	gamepad.BaseName = base
}
