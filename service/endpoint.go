package service

import (
	"encoding/json"
	"gitlab.com/ghostbutler/tool/service"
	"net/http"
)

// define your services ids, must start from last built in index
const (
	// get service name
	APIServiceV1MusicGetName = common.APIServiceType(iota + common.APIServiceBuiltInLast)

	// set service name
	APIServiceV1MusicSetName
)

var MusicAPIService = map[common.APIServiceType]*common.APIEndpoint{
	APIServiceV1MusicGetName: {
		Path:                    []string{"api", "v1", "controller", "gamepads", "list"},
		Method:                  "GET",
		Description:             "get names",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackGetName,
	},
	APIServiceV1MusicSetName: {
		Path:                    []string{"api", "v1", "controller", "gamepads", "name"},
		Method:                  "PUT",
		Description:             "set player name (name)",
		IsMustProvideOneTimeKey: true,
		Callback:                CallbackSetName,
	},
}

// set player name
func CallbackSetName(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		name := common.ExtractFormValue(request,
			"newName")
		if name == "" {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("{\"error\":\"name is empty\""))
			return http.StatusBadRequest
		} else {
			if err := srv.handler.SaveName(name); err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				_, _ = rw.Write([]byte("{\"error\":\"name save failed\""))
				return http.StatusInternalServerError
			}
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"name is empty\""))
		return http.StatusBadRequest
	}
	return http.StatusOK
}

func CallbackGetName(rw http.ResponseWriter,
	_ *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	rw.Header().Add("Content-Type",
		"application/json")
	data, _ := json.Marshal(srv.handler.ExtractNameList())
	_, _ = rw.Write(data)
	return http.StatusOK
}
