package service

import (
	"fmt"
	"github.com/denisbrodbeck/machineid"
	"github.com/splace/joysticks"
	"gitlab.com/ghostbutler/tool/service/rabbit"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	MaximumGamepadCount = 10

	PlayerNameFileName = "name"
	DefaultPlayerName  = "NameIsUnset"
)

type Handler struct {
	rabbitMQController *rabbit.Controller
	machineID          string

	storagePath string

	isRunning bool

	NameMutex sync.Mutex
	Name      string

	sync.Mutex
	GamePad map[int]*Gamepad
}

// build handler
func BuildHandler(storagePath string,
	rabbitMQController *rabbit.Controller) (*Handler, error) {
	handler := &Handler{
		isRunning:          true,
		GamePad:            make(map[int]*Gamepad),
		rabbitMQController: rabbitMQController,
		storagePath:        storagePath,
	}

	// build storage directory
	if err := os.MkdirAll(storagePath,
		0755); err != nil {
		return nil, err
	}

	// calculate machine id
	handler.machineID, _ = machineid.ID()

	// read name file
	if content, err := ioutil.ReadFile(storagePath +
		PlayerNameFileName); err == nil {
		handler.Name = strings.TrimSpace(string(content))
	}
	if handler.Name == "" {
		handler.Name = DefaultPlayerName
	}

	go handler.update()

	return handler, nil
}

func (handler *Handler) update() {
	for handler.isRunning {
		for i := 0; i < MaximumGamepadCount; i++ {
			handler.Lock()
			gamepad, ok := handler.GamePad[i]
			handler.Unlock()
			if ok {
				if !joysticks.DeviceExists(uint8(i)) {
					gamepad.Close()
					handler.Lock()
					delete(handler.GamePad,
						i)
					handler.Unlock()
					fmt.Println("now removing",
						i)
				}
			} else if joysticks.DeviceExists(uint8(i)) {
				if newGamepad, err := BuildGamepad(i,
					handler.rabbitMQController,
					handler.Name+
						"."+
						handler.machineID); err == nil {
					handler.Lock()
					handler.GamePad[i] = newGamepad
					handler.Unlock()
					fmt.Println("now adding",
						i,
						"(",
						newGamepad.ButtonCount,
						"buttons,",
						newGamepad.JoystickCount,
						"joysticks )")
				} else {
					fmt.Println(err)
				}
			}
		}
		time.Sleep(time.Millisecond * 100)
	}
}

// set player name
func (handler *Handler) SaveName(name string) error {
	handler.Name = name
	handler.NameMutex.Lock()
	defer handler.NameMutex.Unlock()
	for _, gamepad := range handler.GamePad {
		gamepad.SetBaseName(handler.Name +
			"." +
			handler.machineID)
	}
	return ioutil.WriteFile(handler.storagePath+
		PlayerNameFileName,
		[]byte(name),
		0664)
}

// extract names
func (handler *Handler) ExtractNameList() []string {
	handler.Lock()
	defer handler.Unlock()
	output := make([]string,
		0,
		1)
	for _, gamepad := range handler.GamePad {
		for i := 1; i <= gamepad.ButtonCount; i++ {
			output = append(output,
				handler.Name+
					"."+
					handler.machineID+
					".gamepad."+
					strconv.Itoa(gamepad.index)+
					".button."+
					strconv.Itoa(i))
		}
		for i := 1; i <= gamepad.JoystickCount; i++ {
			output = append(output,
				handler.Name+
					"."+
					handler.machineID+
					".gamepad."+
					strconv.Itoa(gamepad.index)+
					".joystick."+
					strconv.Itoa(i))
		}
	}
	return output
}
