package service

// we do use this library to declare our endpoints
import (
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/device"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/rabbit"
)

// set here your service version
const (
	VersionMajor = 1
	VersionMinor = 0
)

// this is your service structure, which can contains more then the simple common.Service instance
type Service struct {
	// service
	service *common.Service

	// is running?
	isRunning bool

	// configuration
	configuration *Configuration

	// key manager
	keyManager *keystore.KeyManager

	// directory manager
	directory *common.DirectoryManager

	// handler
	handler *Handler

	// rabbit mq controller
	rabbitMQController *rabbit.Controller
}

// this is the function you will use to build your service
func BuildService(listeningPort int,
	configuration *Configuration) (*Service, error) {
	// allocate service
	service := &Service{
		configuration: configuration,
		isRunning:     true,
		keyManager: keystore.BuildKeyManager(configuration.SecurityManager.Hostname,
			configuration.SecurityManager.Username,
			configuration.SecurityManager.Password,
			common.GhostService[common.ServiceControllerGhostGamepad].Name),
	}

	// build directory manager
	service.directory = common.BuildDirectoryManager(configuration.Directory.Hostname,
		service.keyManager)

	// build rabbit mq connection
	if rabbitMQController, err := rabbit.BuildController(configuration.RabbitMQ.Hostname,
		configuration.RabbitMQ.Username,
		configuration.RabbitMQ.Password); err == nil {
		service.rabbitMQController = rabbitMQController
	} else {
		return nil, err
	}
	if _, err := service.rabbitMQController.Channel.QueueDeclare(rabbit.SensorQueueName,
		true,
		false,
		false,
		false,
		nil); err != nil {
		return nil, err
	}

	// build handler
	if handler, err := BuildHandler( configuration.Storage,
		service.rabbitMQController ); err != nil {
		return nil, err
	} else {
		service.handler = handler
	}

	// build service with previous specified data
	service.service = common.BuildService(listeningPort,
		common.ServiceControllerGhostGamepad,
		MusicAPIService,
		VersionMajor,
		VersionMinor,
		nil,
		[]device.CapabilityType{device.CapabilityTypeGamepad},
		configuration.SecurityManager.Hostname,
		service)

	// service is built
	return service, nil
}

// close service
func (service *Service) Close() {
	service.isRunning = false
	service.rabbitMQController.Close()
}

// is running?
func (service *Service) IsRunning() bool {
	return service.isRunning
}
